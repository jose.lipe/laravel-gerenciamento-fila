<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\ProductUpdating;

class Product extends Model
{
    protected $fillable = [
    	'name',
    	'stock',
    	'stock_max',
    	'price_sale',
    	'price_purchase'
    ];

    protected $events = [
    	'updating' 	=> ProductUpdating::class
    ];
}
