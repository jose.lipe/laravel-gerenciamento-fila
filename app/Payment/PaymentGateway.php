<?php

namespace App\Payment;

use App\Order;
use App\Events\PaymentCompleted;

class PaymentGateway
{
	public function Payment(Order $order)
	{
		\Log::info('Starting payment from Order: '.$order->id);
		sleep(5);
		\Log::info('Ending payment from Order: '.$order->id);
		event(new PaymentCompleted($order));
	}
}