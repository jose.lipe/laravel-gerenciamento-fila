<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\Events\OrderProductsSaveCompleted;

class OrdersController extends Controller
{
    public function create()
    {
    	$products = Product::all()->random(4);
    	return view('orders.create', compact('products'));
    }

    public function store(Request $request)
    {
    	$order = Order::forceCreate([
    		'user_id' => \Auth::user()->id
    	]);

    	foreach ($request->get('products') as $orderProduct) {
    		$product = Product::find($orderProduct['product_id']);
    		OrderProduct::forceCreate([
    			'order_id' => $order->id,
    			'product_id' => $product->id,
    			'price' => $product->price_sale,
    			'quantity' => $orderProduct['quantity']
    		]);
    	}

        event(new OrderProductsSaveCompleted($order));

    	return view('orders.successfully', compact('order'));
    }
}
