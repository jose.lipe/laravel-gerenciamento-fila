<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\StockOutput;

class StockOutputsController extends Controller
{
    public function index()
    {
    	$movements = StockOutput::all();
    	return view('stock-outputs.index', compact('movements'));
    }

    public function create()
    {
    	$products = Product::all()->pluck('name','id');
    	return view('stock-outputs.create', compact('products'));
    }

    public function store(Request $request)
    {
    	$data = array_except($request->all(), '_token');
    	StockOutput::forceCreate($data);
    	return redirect()->route('stock_outputs.index');
    }
}
