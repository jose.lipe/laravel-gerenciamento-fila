<?php

namespace App\Listeners;

use App\Events\OrderCreatedFully;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Payment\PaymentGateway;

class DoPaymentListener implements ShouldQueue
{
    use InteractsWithQueue;

    private $paymentGateway;

    public function __construct(PaymentGateway $paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * Handle the event.
     *
     * @param  OrderCreatedFully  $event
     * @return void
     */
    public function handle(OrderCreatedFully $event)
    {
        $order = $event->getOrder();
        $this->paymentGateway->payment($order);
    }
}
