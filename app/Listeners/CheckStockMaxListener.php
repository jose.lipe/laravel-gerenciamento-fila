<?php

namespace App\Listeners;

use App\Events\ProductUpdating;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\StockGreatherMax;

class CheckStockMaxListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductUpdating  $event
     * @return void
     */
    public function handle(ProductUpdating $event)
    {
        $product = $event->getProduct();

        if($product->stock >= $product->stock_max) {
            \Mail::to('stock@user.com')->queue(new StockGreatherMax($product));
        }
    }
}
