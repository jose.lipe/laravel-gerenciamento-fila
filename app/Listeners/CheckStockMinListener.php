<?php

namespace App\Listeners;

use App\Events\ProductUpdating;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\StockLessMin;

class CheckStockMinListener
{

    /**
     * Handle the event.
     *
     * @param  ProductUpdating  $event
     * @return void
     */
    public function handle(ProductUpdating $event)
    {
        $product = $event->getProduct();

        if($product->stock < ($product->stock_max * 0.1)) {
            \Mail::to('stock@user.com')->queue(new StockLessMin($product));
        }
    }
}
