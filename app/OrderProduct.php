<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\OrderProductCreated;

class OrderProduct extends Model
{
    protected $casts = [
    	'price' => 'float',
    	'quantity' => 'integer'
    ];

    protected $events = [
    	'created' => OrderProductCreated::class
    ];

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}
