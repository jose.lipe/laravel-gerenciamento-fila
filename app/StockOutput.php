<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\StockOutputCreated;

class StockOutput extends Model
{
    use StockMovements;

    protected $events = [
    	'created' => StockOutputCreated::class
    ];
}
